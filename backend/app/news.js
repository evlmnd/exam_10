const express = require('express');
const fs = require('fs');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const newsDb = require('../fileDb/NewsDb');
const commentsDb = require('../fileDb/CommentsDb');

const newsFile = './fileDb/newsDb.json';
const news = express.Router();


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

news.post('/news', upload.single('image'), (req, res) => {
    if(req.file) {
        req.body.image = req.file.filename;
    }
    if (req.body.title === '' || req.body.content === '') {
        res.sendStatus(400);
    } else {
        req.body.id = nanoid();
        req.body.date = new Date();
        newsDb.addItem(req.body);
        res.send(req.body);
    }
});

news.get('/news', (req, res) => {
    const newsList = fs.readFileSync(newsFile);
    const news = [];

    JSON.parse(newsList).map(newsItem => {
        delete newsItem['content'];  //?
        news.push(newsItem);
    });
    res.send(news);
});

news.get('/news/:id', (req, res) => {
    const newsList = JSON.parse(fs.readFileSync(newsFile));
    let newsItem;

    for (let i = 0; i < newsList.length; i++) {
        if (newsList[i].id === req.params.id) {
            newsItem = newsList[i];
        }
    }
    res.send(newsItem);
});

news.delete('/news/:id', (req, res) => {
    newsDb.deleteItem(req.params.id, newsFile);

    const commentsList = JSON.parse(fs.readFileSync('./fileDb/commentsDb.json'));
    for (let i = 0; i < commentsList.length; i++) {
        if (commentsList[i].newsItemId === req.params.id) {
            console.log(commentsList[i].id);
            commentsDb.deleteItem(commentsList[i].id)
        }
    }

    res.send('ok');
});

module.exports = news;