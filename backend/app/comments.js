const express = require('express');
const nanoid = require('nanoid');
const fs = require('fs');
const commentsDb = require('../fileDb/CommentsDb');
const comments = express.Router();

const commentsFile = './fileDb/commentsDb.json';

comments.post('/comments', (req, res) => {
    if (req.body.author === '') {
        req.body.author = 'Anonimus'
    }
    if (req.body.content === '' || !req.body.newsItemId) {
        res.sendStatus(400);
    }

    const newsList = JSON.parse(fs.readFileSync('./fileDb/newsDb.json'));
    let newsItemId = null;
    for (let i = 0; i < newsList.length; i++) {
        if (newsList[i].id === req.body.newsItemId) {
            newsItemId = newsList[i].id;
            console.log(newsItemId);
        }
    }
    if (newsItemId !== null) {
        req.body.id = nanoid();
        commentsDb.addItem(req.body);
        res.send(req.body);
    } else {
        res.sendStatus(400);
    }
});

comments.get('/comments', (req, res) => {
    const commentsList = JSON.parse(fs.readFileSync(commentsFile));
    const comments = [];
    if (req.query.news_id) {
        for (let i = 0; i < commentsList.length; i++) {
            if (req.query.news_id === commentsList[i].newsItemId) {
                comments.push(commentsList[i]);
            }
        }
        res.send(comments);
    } else {
        res.send(commentsList);
    }
});

comments.delete('/comments/:id', (req, res) => {
    commentsDb.deleteItem(req.params.id);
    res.send('ok');
});

module.exports = comments;