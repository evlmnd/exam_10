const fs = require('fs');

const fileName = './fileDb/commentsDb.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(fileName);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },

    deleteItem(id) {
        const fileContents = fs.readFileSync(fileName);
        data = JSON.parse(fileContents);
        for (let i = 0; i < data.length; i++) {
            if (data[i].id === id) {
                data.splice(i, 1);
            }
        }
        this.save(fileName);
    },

    addItem(item) {
        data.push(item);
        this.save();
    },

    save() {
        fs.writeFileSync(fileName, JSON.stringify(data, null, 2));
    }
};