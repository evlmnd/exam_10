const express = require('express');
const news = require('./app/news');
const comments = require('./app/comments');
const newsDb = require('./fileDb/NewsDb');
const commentsDb = require('./fileDb/CommentsDb');
const cors = require('cors');

const app = express();
newsDb.init();
commentsDb.init();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());
app.use(news);
app.use(comments);

const port = 8000;

app.listen(port, () => {
    console.log(`Server starts on ${port} port`);
});