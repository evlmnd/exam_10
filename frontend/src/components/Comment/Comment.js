import React, {Component} from 'react';

class Comment extends Component {
    render() {
        return (
            <div>
                <p>[ {this.props.author} ]: {this.props.content}</p>
                <span onClick={this.props.deleteClick} className="DeleteButton">Delete</span>
            </div>
        );
    }
}

export default Comment;