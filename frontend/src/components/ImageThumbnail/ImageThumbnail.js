import React from 'react';
import {apiURL} from "../../constants";
import defaultImage from '../../assets/images/default-thumbnail.jpg';


const styles = {
    width: '100px',
    height: 'auto',
    marginRight: '10px'
};

const ImageThumbnail = (props) => {
    if (props.image) {
        const image = apiURL + '/uploads/' + props.image;
        return (<img src={image} alt="img" style={styles}/>);
    } else {
        return (<img src={defaultImage} alt="img" style={styles}/>);
    }
};

export default ImageThumbnail;