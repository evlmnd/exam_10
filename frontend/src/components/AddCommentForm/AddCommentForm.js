import React, {Component} from 'react';
import {connect} from "react-redux";
import {getComments, sendComment} from "../../store/actions";

class AddCommentForm extends Component {

    state = {
        author: '',
        content: ''
    };

    inputChangeHandler = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };


    onSubmitForm = event => {
        event.preventDefault();

        const formData = {newsItemId: this.props.newsItemId, author: this.state.author, content: this.state.content};
        this.props.onSubmit(formData);

        this.setState({author: '', content: ''});
    };

    render() {
        return (
            <div>
                <h4>Add Comment</h4>
                <form onSubmit={this.onSubmitForm}>
                    <input type="text" name="author" placeholder="Author" value={this.state.author} onChange={this.inputChangeHandler}/>
                    <input type="text" name="content" placeholder="Comment" value={this.state.content} onChange={this.inputChangeHandler} required/>
                    <button type="submit">Add</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: formData => dispatch(sendComment(formData)),
        getComments: () => dispatch(getComments())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCommentForm);