import React from 'react';
import {Nav, Navbar, NavLink} from "reactstrap";
import{NavLink as RouterNavLink} from 'react-router-dom';

const ToolBar = () => {
    return (
        <Navbar color="light" light expand="md">
                {/*<NavbarBrand tag={RouterNavLink} to="/">News</NavbarBrand>*/}

                <Nav className="ml-auto" navbar>
                                <NavLink tag={RouterNavLink} to="/" exact>News</NavLink>
                                <NavLink tag={RouterNavLink} to="/addpost" exact>Add Post</NavLink>

                </Nav>
        </Navbar>
    );
};

export default ToolBar;