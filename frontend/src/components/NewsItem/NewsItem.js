import React, {Component} from 'react';
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import './NewsItem.css';
import ImageThumbnail from "../ImageThumbnail/ImageThumbnail";

class NewsItem extends Component {
    render() {
        return (
            <div className="NewsItem">
                <div className="Image">
                    <ImageThumbnail image={this.props.image}/>
                </div>
                <div className="InfoBox">
                    <h3>{this.props.title}</h3>
                    <p className="ItemInfo">
                        <span>{this.props.date}</span>
                        <span className="ReadMore">
                            <NavLink tag={RouterNavLink} to={"/news/" + this.props.id} exact>Read Full Post >> ...</NavLink>
                        </span>
                        <span onClick={this.props.deleteClick} className="DeleteButton">Delete</span>
                    </p>
                </div>
            </div>
        );
    }
}

export default NewsItem;