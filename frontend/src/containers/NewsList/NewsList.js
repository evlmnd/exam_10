import React, {Component} from 'react';
import NewsItem from "../../components/NewsItem/NewsItem";
import {connect} from "react-redux";
import {deleteNewsItem, getNews} from "../../store/actions";

class NewsList extends Component {

    componentDidMount() {
        this.props.getNews();
    }

    componentDidUpdate() {
        this.props.getNews();
    }

    render() {
        return (
            <div className="NewsList">
                {this.props.news.map(item => {
                    const date = (item.date.slice(0, 10) + ', ' + item.date.slice(11, 16));
                    return <NewsItem
                        title={item.title}
                        date={date}
                        image={item.image}
                        id={item.id}
                        key={item.id}
                        deleteClick={() => this.props.onDeleteNewsItem(item.id)}
                    />
                })}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        news: state.news,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getNews: () => dispatch(getNews()),
        onDeleteNewsItem: (itemId) => dispatch(deleteNewsItem(itemId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);