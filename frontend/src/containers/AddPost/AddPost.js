import React, {Component} from 'react';
import {connect} from "react-redux";
import {addPost} from "../../store/actions";

class AddPost extends Component {
    state = {
        title: '',
        content: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.addPost(formData).then(() => this.props.history.push('/'));
    };

    inputChangeHandler = event => {
        event.preventDefault();

        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        event.preventDefault();

        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <div>
                <form onSubmit={this.submitFormHandler}>
                    <input type="text" placeholder="Title" name="title" value={this.state.title} onChange={this.inputChangeHandler} required/>
                    <input type="text" placeholder="Content" name="content" value={this.state.content} onChange={this.inputChangeHandler} required/>
                    <input
                        placeholder="Add file"
                        type="file"
                        name="image" id="image"
                        onChange={this.fileChangeHandler}
                    />
                    <button type="submit">Save</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.loading
    };
};


const mapDispatchToProps = dispatch => {
        return {
            addPost: data => dispatch(addPost(data))
        }
    }
;

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);