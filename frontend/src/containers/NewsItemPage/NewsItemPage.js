import React, {Component, Fragment} from 'react';
import {deleteComment, getComments, getNewsItem} from "../../store/actions";
import {connect} from "react-redux";
import Comment from "../../components/Comment/Comment";
import AddCommentForm from "../../components/AddCommentForm/AddCommentForm";
import {apiURL} from "../../constants";
import './NewsItemPage.css';


class NewsItemPage extends Component {

    onDeleteComment = (id) => {
        this.props.deleteComment(id);
            this.props.getComments(this.props.match.params.id);
    };

    componentDidMount() {
        this.props.getNewsItem(this.props.match.params.id);
        this.props.getComments(this.props.match.params.id);
    }

    componentDidUpdate() {
        this.props.getComments(this.props.match.params.id);
    }

    render() {
        const item = this.props.newsItem;
        let time = null;
        if (this.props.newsItem.date) {
            time = this.props.newsItem.date.slice(0, 10) + ', ' + this.props.newsItem.date.slice(11, 16)
        }

        return (
            <Fragment>
                <div className="Contents">
                    {item.image ? <img src={apiURL + '/uploads/' + item.image} alt="News" className="image"/> : null}
                    <h2>{item.title}</h2>
                    <p>{time}</p>
                    <p>{item.content}</p>
                    <h4>Comments</h4>
                    {this.props.comments.length > 0 ?
                        this.props.comments.map(comment => {
                            return <Comment
                                author={comment.author}
                                content={comment.content}
                                key={comment.id}
                                deleteClick={() => this.onDeleteComment(comment.id)}
                            />
                        }) : <p>Write first comment!</p>}
                </div>
                <AddCommentForm newsItemId={item.id}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        newsItem: state.newsItem,
        comments: state.comments
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getNewsItem: (id) => dispatch(getNewsItem(id)),
        getComments: (id) => dispatch(getComments(id)),
        deleteComment: (id) => dispatch(deleteComment(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsItemPage);