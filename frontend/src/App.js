import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import NewsList from "./containers/NewsList/NewsList";
import NewsItemPage from "./containers/NewsItemPage/NewsItemPage";
import AddPost from "./containers/AddPost/AddPost";
import ToolBar from "./components/ToolBar/ToolBar";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Container>
            <header>
                <ToolBar/>
            </header>
            <Switch>
                <Route path="/" exact component={NewsList}/>
                <Route path="/news/:id" exact component={NewsItemPage}/>
                <Route path="/addpost" exact component={AddPost}/>
            </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
