import {
    GET_NEWS_SUCCESS,
    GET_NEWS_ITEM_SUCCESS,
    GET_COMMENTS_SUCCESS
} from "./actions";

const initialState = {
    news: [],
    newsItem: {},
    comments: [],
    error: null
    // loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_NEWS_SUCCESS:
            return {...state, news: action.news};
        case GET_NEWS_ITEM_SUCCESS:
            return {...state, newsItem: action.item};
        case GET_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};
        default:
            return state;
    }
};

export default reducer;