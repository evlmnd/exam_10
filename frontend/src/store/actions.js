import axios from '../axios-news';

export const GET_NEWS_SUCCESS = 'GET_NEWS_SUCCESS';
export const GET_NEWS_ITEM_SUCCESS = 'GET_NEWS_ITEM_SUCCESS';
export const GET_COMMENTS_SUCCESS = 'GET_COMMENTS_SUCCESS';
export const LOADING_START = 'LOADING_START';
export const LOADING_SUCCESS = 'LOADING_SUCCESS';


export const getNewsSuccess = (news) => ({type: GET_NEWS_SUCCESS, news});
export const getNewsItemSuccess = (item) => ({type: GET_NEWS_ITEM_SUCCESS, item});
export const getCommentsSuccess = (comments) => ({type: GET_COMMENTS_SUCCESS, comments});
export const loadingStart = () => ({type: LOADING_START});
export const loadingSuccess = () => ({type: LOADING_SUCCESS});

export const getNews = () => {
    return dispatch => {
        return axios.get('/news').then(response => {
            dispatch(getNewsSuccess(response.data))
        });
    };
};

export const deleteNewsItem = (itemId) => {
    return dispatch => {
        return axios.delete('/news/' + itemId).then(() => {
            dispatch(getNews());
        });
    };
};

export const getNewsItem = (id) => {
    return dispatch => {
        return axios.get('/news/' + id).then(response => {
            dispatch(getNewsItemSuccess(response.data))
        });
    };
};

export const getComments = (id) => {
    return dispatch => {
        return axios.get('/comments/?news_id=' + id).then(response => {
            dispatch(getCommentsSuccess(response.data));
        });
    };
};

export const deleteComment = (id) => {
    return dispatch => {
        axios.delete('/comments/' + id);
    }
};

export const sendComment = (data) => {
    return dispatch => {
        axios.post('/comments', data).then(() => {
            dispatch(getComments(data.newsItemId))
        })
    }
};

export const addPost = (data) => {
    return dispatch => {
        dispatch(loadingStart());
        return axios.post('/news', data);
    }
};